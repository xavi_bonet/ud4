package Ejercicios;

public class Ejercicio5App {

	public static void main(String[] args) {
		
		int a = 1;
		int b = 2;
		int c = 3;
		int d = 4;
		
		System.out.println("A = " + a);
		System.out.println("B = " + b);
		System.out.println("C = " + c);
		System.out.println("D = " + d);
		
		b = c;
		System.out.println("B toma el valor de C");
		System.out.println("B = " + b);	
		
		c = a;
		System.out.println("C toma el valor de A");
		System.out.println("C = " + c);	
		
		a = d;
		System.out.println("A toma el valor de D");
		System.out.println("A = " + d);	
		
		d = b;
		System.out.println("D toma el valor de B");
		System.out.println("D = " + b);	
	}

}
