package Ejercicios;

public class Ejercicio3App {

	public static void main(String[] args) {
		
		int X = 13;
		int Y = 21;
		double N = 27.589;
		double M = 14.76;
		
		//Valor varibles
		System.out.println("X = " + X);
		System.out.println("Y = " + Y);
		System.out.println("N = " + N);
		System.out.println("M = " + M);
		
		//La suma X + Y
		System.out.println("X + Y = " + (X+Y));
		
		//La diferencia X - Y
		System.out.println("X - Y = " + (X-Y));
		
		//EL Producto X * Y
		System.out.println("X x Y = " + (X*Y));
		
		//El cociente X / Y
		System.out.println("X / Y = " + ((float)X/Y));
		
		//El resto X % Y
		System.out.println("X % Y = " + (X%Y));
		
		//La suma N + M
		System.out.println("N + M = " + (N+M));
		
		//La diferencia N � M
		System.out.println("N - M = " + (N-M));
		
		//El producto N * M
		System.out.println("N x M = " + (N*M));
		
		//El cociente N / M
		System.out.println("N / M = " + (N/M));
		
		//El resto N % M
		System.out.println("N % M = " + (N%M));
		
		//La suma X + N
		System.out.println("X + N = " + (X+N));
		
		//El cociente Y / M
		System.out.println("Y / M = " + (Y/M));
		
		//El resto Y % M
		System.out.println("Y % M = " + (Y%M));
		
		//El doble de cada variable
		System.out.println("El doble de X = " + (X*2));
		System.out.println("El doble de Y = " + (Y*2));
		System.out.println("El doble de N = " + (N*2));
		System.out.println("El doble de M = " + (M*2));
		
		
		//La suma de todas las variables
		System.out.println("X + Y + N + M = " + (X+Y+N+M));
		
		//El producto de todas las variables
		System.out.println("X * Y * N * M = " + (X*Y*N*M));
	}

}
