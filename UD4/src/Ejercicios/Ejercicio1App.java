package Ejercicios;

public class Ejercicio1App {
	
	public static void main(String args[]){
		
	int num1 = 10;
	int num2 = 15;
	float a;
	
	// Suma
	System.out.println(num1 + " + " + num2 + " = " + (num1+num2) );
	
	// Resta
	System.out.println(num1 + " - " + num2 + " = " + (num1-num2) );
	
	// Multiplicación
	System.out.println(num1 + " x " + num2 + " = " + (num1*num2) );
	
	// División
	a = (float)num1/num2;
	System.out.println(num1 + " / " + num2 + " = " + a );
	
	// Módulo
	System.out.println(num1 + " % " + num2 + " = " + (num1%num2) );
	
	}
	
}
