package Ejercicios;

public class Ejercicio2App {
	
	public static void main(String args[]){
	
		int N = 3;
		double A = 7.5;
		char C = 'f';
		
		System.out.println("N = " + N);
		System.out.println("A = " + A);
		System.out.println("C = " + C);
		
		System.out.println("N + A = " + (N+A));
		System.out.println("A - N = " + (A-N));
		System.out.println("Valor num�rico correspondiente a '" + C + "' = " + (int)C);
		
	}
	
}
